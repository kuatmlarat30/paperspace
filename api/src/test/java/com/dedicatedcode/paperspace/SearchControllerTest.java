package com.dedicatedcode.paperspace;

import com.dedicatedcode.paperspace.model.Tag;
import com.dedicatedcode.paperspace.search.SolrService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(SearchController.class)
class SearchControllerTest {
    @MockBean
    private SolrService solrService;
    @MockBean
    private TagService tagService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnMostRecentDocuments() throws Exception {
        when(solrService.recent(0, 20)).thenReturn(TestHelper.createSearchResponse(null, 20));
        mockMvc.perform(get("/api/search.json")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }

    @Test
    void shouldReturnRedirectWhenHavingUnknownTags() throws Exception {
        UUID knownTag = UUID.randomUUID();
        UUID unknownTag = UUID.randomUUID();
        when(this.tagService.get(knownTag)).thenReturn(new Tag(knownTag, "Known Tag"));
        when(this.tagService.get(unknownTag)).thenReturn(null);

        mockMvc.perform(get("/api/search.json")
                        .param("tags", knownTag.toString(), unknownTag.toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(307))
                .andExpect(header().string("Location", "/api/search.json?tags=" + knownTag));

        mockMvc.perform(get("/api/search.json")
                        .param("tags", knownTag.toString(), unknownTag.toString())
                        .param("q", "queryString")
                        .param("page-size", "75")
                        .param("page", "3")
                        .param("filters", "OPEN_TASKS")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(307))
                .andExpect(header().string("Location", "/api/search.json?q=queryString&page=3&page-size=75&tags=" + knownTag + "&filters=OPEN_TASKS"));
    }

    @Test
    void shouldHandleGivenPageSize() throws Exception {
        UUID knownTag = UUID.randomUUID();
        when(this.tagService.get(knownTag)).thenReturn(new Tag(knownTag, "Known Tag"));
        when(solrService.query(null, Collections.singletonList(knownTag), Collections.emptyList(), 0, 100)).thenReturn(TestHelper.createSearchResponse(null, 100));

        mockMvc.perform(get("/api/search.json")
                        .param("tags", knownTag.toString())
                        .param("page-size", "100")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.items.length()", is(100)));
    }
}
