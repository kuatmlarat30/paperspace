package com.dedicatedcode.paperspace.search;

import com.dedicatedcode.paperspace.SearchFilter;
import com.dedicatedcode.paperspace.web.DocumentResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaginationService {
    public Map<String, Object> createPagination(String queryString, int page, int resultsPerPage, long totalDocumentsFound, List<DocumentResponse> documents, List<SearchFilter> filters, List<UUID> tagIds) {
        long totalPages = totalDocumentsFound <= resultsPerPage ? 1 : (totalDocumentsFound / resultsPerPage);
        totalPages += totalDocumentsFound > resultsPerPage && totalDocumentsFound % resultsPerPage > 0 ? 1 : 0;

        Map<String, Object> pagination = new HashMap<>();
        if (page > 0) {
            pagination.put("previous", createSearchLink(queryString, tagIds, filters, page - 1));
        }
        if (page < totalPages - 1) {
            pagination.put("next", createSearchLink(queryString, tagIds, filters, page + 1));
        }
        List<String> pageLinks = new ArrayList<>();
        for (int i = 0; i < totalPages; i++) {
            pageLinks.add(createSearchLink(queryString, tagIds, filters, i));
        }
        pagination.put("pages", pageLinks);
        pagination.put("page", page);
        pagination.put("startIndex", page * resultsPerPage + 1);
        pagination.put("endIndex", page * resultsPerPage + documents.size());
        pagination.put("results", totalDocumentsFound);
        pagination.put("totalPages", totalPages);
        return pagination;
    }

    private String createSearchLink(String queryString, List<UUID> tagIds, List<SearchFilter> filters, int page) {
        String link = "/api/search.json?";
        if (!StringUtils.isEmpty(queryString)) {
            link += "q=" + queryString + "&";
        }
        if (page > 0) {
            link += "page=" + page + "&";
        }
        if (!tagIds.isEmpty()) {
            link += "tags=" + tagIds.stream().map(UUID::toString).collect(Collectors.joining(",")) + "&";
        }
        if (!filters.isEmpty()) {
            link += "filters=" + filters.stream().map(Enum::name).collect(Collectors.joining(",")) + "&";
        }
        return link.substring(0, link.length() - 1);
    }

}
